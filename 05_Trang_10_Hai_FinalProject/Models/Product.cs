﻿using _05_Trang_10_Hai_FinalProject.EnumValue;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace _05_Trang_10_Hai_FinalProject.Models;

public partial class Product
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string? Name { get; set; }

    [Column("category_id"), DisplayName("Category")]
    public int? CategoryId { get; set; }

    [Column("unit_id"), DisplayName("Unit")]
    public ProductUnit? UnitId { get; set; }

    [Column("customer_id"), DisplayName("Customer")]
    public int? CustomerId { get; set; }

    public virtual Category? Category { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual ICollection<ExportInfo> ExportInfos { get; set; } = new List<ExportInfo>();

    public virtual ICollection<ImportInfo> ImportInfos { get; set; } = new List<ImportInfo>();
}
