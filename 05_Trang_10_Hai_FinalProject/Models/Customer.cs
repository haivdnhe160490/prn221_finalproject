﻿using _05_Trang_10_Hai_FinalProject.EnumValue;
using System.ComponentModel.DataAnnotations.Schema;

namespace _05_Trang_10_Hai_FinalProject.Models;

public partial class Customer
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string? Name { get; set; }

    [Column("address")]
    public string? Address { get; set; }

    [Column("phone")]
    public int? Phone { get; set; }

    [Column("email")]
    public string? Email { get; set; }

    [Column("note")]
    public string? Note { get; set; }

    [Column("contract_date")]
    public DateTime? ContractDate { get; set; }

    [Column("type")]
    public CustomerType? Type { get; set; }

    [Column("created_by")]
    public int? CreatedBy { get; set; }

    [Column("created_at")]
    public DateTime? CreatedAt { get; set; }

    [Column("update_at")]
    public DateTime? UpdateAt { get; set; }

    [Column("update_by")]
    public int? UpdateBy { get; set; }

    public virtual User CreatedByNavigation { get; set; }

    public virtual ICollection<Export> Exports { get; set; } = new List<Export>();

    public virtual ICollection<Import> Imports { get; set; } = new List<Import>();

    public virtual ICollection<Product> Products { get; set; } = new List<Product>();

    public virtual User UpdateByNavigation { get; set; }
}
