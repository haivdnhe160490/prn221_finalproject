﻿using System.ComponentModel.DataAnnotations.Schema;

namespace _05_Trang_10_Hai_FinalProject.Models;

public partial class Category
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string? Name { get; set; }

    [Column("note")]
    public string? Note { get; set; }

    public virtual ICollection<Product> Products { get; set; } = new List<Product>();
}
