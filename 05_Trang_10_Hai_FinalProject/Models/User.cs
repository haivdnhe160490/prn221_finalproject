﻿using _05_Trang_10_Hai_FinalProject.EnumValue;
using System.ComponentModel.DataAnnotations.Schema;

namespace _05_Trang_10_Hai_FinalProject.Models;

public partial class User
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string? Name { get; set; }

    [Column("email")]
    public string? Email { get; set; }

    [Column("password")]
    public string? Password { get; set; }

    [Column("role")]
    public UserRole? Role { get; set; }

    [Column("created_at")]
    public DateTime? CreatedAt { get; set; }

    [Column("created_by")]
    public int? CreatedBy { get; set; }

    [Column("update_at")]
    public DateTime? UpdateAt { get; set; }

    [Column("update_by")]
    public int? UpdateBy { get; set; }

    [Column("status")]
    public AccountStatus? Status { get; set; }


    public virtual User CreatedByNavigation { get; set; }

    public virtual ICollection<Customer> CustomerCreatedByNavigations { get; set; } = new List<Customer>();

    public virtual ICollection<Customer> CustomerUpdateByNavigations { get; set; } = new List<Customer>();

    public virtual ICollection<Export> ExportCreatedByNavigations { get; set; } = new List<Export>();

    public virtual ICollection<Export> ExportUpdateByNavigations { get; set; } = new List<Export>();

    public virtual ICollection<Import> ImportCreatedByNavigations { get; set; } = new List<Import>();

    public virtual ICollection<Import> ImportUpdateByNavigations { get; set; } = new List<Import>();

    public virtual ICollection<User> InverseCreatedByNavigation { get; set; } = new List<User>();

    public virtual ICollection<User> InverseUpdateByNavigation { get; set; } = new List<User>();

    public virtual User UpdateByNavigation { get; set; }
}