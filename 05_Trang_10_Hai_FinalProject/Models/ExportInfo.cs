﻿using System.ComponentModel.DataAnnotations.Schema;

namespace _05_Trang_10_Hai_FinalProject.Models;

public partial class ExportInfo
{
    [Column("id")]
    public int Id { get; set; }

    [Column("product_id")]
    public int ProductId { get; set; }

    [Column("export_id")]
    public int ExportId { get; set; }

    [Column("cout")]
    public int? Cout { get; set; }

    public virtual Export Export { get; set; }

    public virtual Product Product { get; set; }
}
