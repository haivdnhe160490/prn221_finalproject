﻿using System.ComponentModel.DataAnnotations.Schema;

namespace _05_Trang_10_Hai_FinalProject.Models;

public partial class ImportInfo
{
    [Column("id")]
    public int Id { get; set; }

    [Column("product_id")]
    public int ProductId { get; set; }

    [Column("import_id")]
    public int ImportId { get; set; }

    [Column("count")]
    public int? Count { get; set; }

    [Column("in_price")]
    public decimal? InPrice { get; set; }

    [Column("out_price")]
    public decimal? OutPrice { get; set; }

    public virtual Import Import { get; set; } = null!;

    public virtual Product Product { get; set; } = null!;
}
