﻿using _05_Trang_10_Hai_FinalProject.EnumValue;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace _05_Trang_10_Hai_FinalProject.Models;

public partial class Import
{
    [Column("id")]
    public int Id { get; set; }

    [Column("code")]
    public string? Code { get; set; }

    [Column("status")]
    public ProductStatus? Status { get; set; }

    [Column("created_at")]
    public DateTime? CreatedAt { get; set; }

    [Column("created_by")]
    public int? CreatedBy { get; set; }

    [Column("update_by")]
    public int? UpdateBy { get; set; }

    [Column("update_at")]
    public DateTime? UpdateAt { get; set; }

    [Column("customer_id"), DisplayName("Customer")]
    public int? CustomerId { get; set; }

    public virtual User CreatedByNavigation { get; set; }

    public virtual Customer Customer { get; set; }

    public virtual List<ImportInfo> ImportInfos { get; set; }

    public virtual User UpdateByNavigation { get; set; }
}
