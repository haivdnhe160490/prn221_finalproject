﻿using _05_Trang_10_Hai_FinalProject.Data;
using _05_Trang_10_Hai_FinalProject.EnumValue;
using _05_Trang_10_Hai_FinalProject.Models;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace _05_Trang_10_Hai_FinalProject.Controllers
{
    [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.SALER)}")]
    public class CategoriesController : Controller
    {
        private readonly Context _context;
        private readonly INotyfService _notyf;

        public CategoriesController(Context context, INotyfService notyf)
        {
            _context = context;
            _notyf = notyf;
        }

        // GET: Categories
        public async Task<IActionResult> Index(string? CategoryName)
        {
            if (_context.Categories == null)
            {
                return Problem("Entity set 'Context.Category'  is null.");
            }

            var category = await _context.Categories.Include(c => c.Products).ToListAsync();

            if (CategoryName != null)
            {
                category = category.Where(c => c.Name.ToLower().Contains(CategoryName.ToLower())).ToList();
            }
            return View(category);

        }

        // GET: Categories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Categories == null)
            {
                _notyf.Warning("Category not exist");
                return RedirectToAction(nameof(Index));
            }

            var category = await _context.Categories.Include(c => c.Products)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (category == null)
            {
                _notyf.Warning("Category not exist");
                return RedirectToAction(nameof(Index));
            }

            return View(category);
        }

        // GET: Categories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Note")] Category category)
        {
            _context.Add(category);
            await _context.SaveChangesAsync();
            _notyf.Success("Create category succeed");
            return RedirectToAction(nameof(Index));
        }

        // GET: Categories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Categories == null)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            var category = await _context.Categories.Include(c => c.Products).FirstOrDefaultAsync(c => c.Id == id);
            if (category == null)
            {
                _notyf.Warning("Category not exist");
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Note")] Category category)
        {
            if (id != category.Id)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            try
            {
                _context.Update(category);
                _notyf.Success("Update succeed");
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(category.Id))
                {
                    _notyf.Warning("Category not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Categories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Categories == null)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            var category = await _context.Categories
                .FirstOrDefaultAsync(m => m.Id == id);
            if (category == null)
            {
                _notyf.Warning("Category not exist");
                return RedirectToAction(nameof(Index));
            }

            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Categories == null)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }
            var category = await _context.Categories.FindAsync(id);
            if (category != null)
            {
                _context.Categories.Remove(category);
                _notyf.Success("Delete succeed");
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CategoryExists(int id)
        {
            return (_context.Categories?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
