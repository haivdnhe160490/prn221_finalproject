﻿using _05_Trang_10_Hai_FinalProject.Data;
using _05_Trang_10_Hai_FinalProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace _05_Trang_10_Hai_FinalProject.Controllers
{
    using _05_Trang_10_Hai_FinalProject.EnumValue;
    using AspNetCoreHero.ToastNotification.Abstractions;
    using BCrypt.Net;
    using Microsoft.AspNetCore.Authorization;

    public class UsersController : Controller
    {
        private readonly string nameidentifier = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        private readonly int key = 13;
        private readonly string pass = "123";
        private readonly Context _context;
        private readonly INotyfService _notyf;

        public UsersController(Context context, INotyfService notyf)
        {
            _context = context;
            _notyf = notyf;
        }

        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public async Task<IActionResult> Index(string? CreatedUserAt, string? UpdatedUserAt, string? emailUser, UserRole? roleUser)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'Context.User'  is null.");
            }

            var cus = await _context.Users.Include(c => c.UpdateByNavigation).Include(c => c.CreatedByNavigation).ToListAsync();
            if (CreatedUserAt != null)
            {
                string createStartDate = CreatedUserAt.Split('-')[0].Trim();
                string createEndDate = CreatedUserAt.Split('-')[1].Trim();
                DateTime StartDate = new DateTime(int.Parse(createStartDate.Split("/")[2]), int.Parse(createStartDate.Split("/")[1]), int.Parse(createStartDate.Split("/")[0]));
                DateTime EndDate = new DateTime(int.Parse(createEndDate.Split("/")[2]), int.Parse(createEndDate.Split("/")[1]), int.Parse(createEndDate.Split("/")[0]));
                cus = cus.Where(c => c.CreatedAt >= StartDate && c.CreatedAt <= EndDate).ToList();
            }
            if (UpdatedUserAt != null)
            {
                string UpdatedStartDate = UpdatedUserAt.Split('-')[0].Trim();
                string UpdatedEndDate = UpdatedUserAt.Split('-')[1].Trim();
                DateTime StartDate = new DateTime(int.Parse(UpdatedStartDate.Split("/")[2]), int.Parse(UpdatedStartDate.Split("/")[1]), int.Parse(UpdatedStartDate.Split("/")[0]));
                DateTime EndDate = new DateTime(int.Parse(UpdatedEndDate.Split("/")[2]), int.Parse(UpdatedEndDate.Split("/")[1]), int.Parse(UpdatedEndDate.Split("/")[0]));
                cus = cus.Where(c => c.UpdateAt >= StartDate && c.UpdateAt <= EndDate).ToList();
            }

            if (emailUser != null)
            {
                cus = cus.Where(c => c.Email.Contains(emailUser)).ToList();
            }
            if (roleUser != null)
            {
                cus = cus.Where(c => c.Role == roleUser).ToList();
            }
            cus.RemoveAll(c => c.Id == int.Parse(User.FindFirst(nameidentifier).Value));
            return View(cus);
            //return _context.User != null ? 
            //              View(await _context.User.ToListAsync()) :
            //              Problem("Entity set 'Context.User'  is null.");
        }

        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Users == null)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            var user = await _context.Users.Include(c => c.UpdateByNavigation).Include(c => c.CreatedByNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                _notyf.Warning("User not exist");
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public async Task<IActionResult> Create([Bind("Id,Name,Email,Password,Role,Status")] User user)
        {
            var listUsers = await _context.Users.ToListAsync();
            if (listUsers.Any(u => u.Email == user.Email)){
                _notyf.Warning("Email user not exist");
                return View(user);
            }
            user.CreatedBy = int.Parse(User.FindFirst(nameidentifier).Value);
            user.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            user.CreatedAt = DateTime.Now;
            user.UpdateAt = DateTime.Now;
            user.Password = BCrypt.EnhancedHashPassword(user.Password, key);
            _context.Add(user);
            await _context.SaveChangesAsync();
            _notyf.Success("Create user succeed");
            return RedirectToAction(nameof(Index));

        }

        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Users == null)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            var user = await _context.Users.Include(c => c.UpdateByNavigation).Include(c => c.CreatedByNavigation).FirstOrDefaultAsync(u => u.Id == id);
            if (user == null)
            {
                _notyf.Warning("User not exist");
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Role,Email,Status")] User user)
        {
            if (id != user.Id)
            {
                _notyf.Warning("User not exist");
                return RedirectToAction(nameof(Index));
            }
            var oldUser = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);

            if (oldUser == null)
            {
                _notyf.Warning("User not exist");
                return RedirectToAction(nameof(Index));
            }
            oldUser.Status = user.Status;
            oldUser.Role = user.Role;
            oldUser.Email = user.Email;
            oldUser.UpdateAt = DateTime.Now;
            oldUser.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            try
            {
                _context.Update(oldUser);
                await _context.SaveChangesAsync();
                _notyf.Success("Update Success");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(user.Id))
                {
                    _notyf.Warning("User not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Users == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'Context.User'  is null.");
            }
            var user = await _context.Users.FindAsync(id);
            if (user != null)
            {
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();
                _notyf.Success("Remove user succeed!!");
            }
            else
            {
                _notyf.Warning("User not exist");
                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public async Task<IActionResult> ResetPass(int? id)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'Context.User'  is null.");
            }
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            user.Password = BCrypt.EnhancedHashPassword(pass, key);
            try
            {
                _context.Update(user);
                await _context.SaveChangesAsync();
                _notyf.Success("Reset password succeed!");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(user.Id))
                {
                    _notyf.Warning("User not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return Redirect($"/Users/Edit/{id}");
        }

        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public async Task<IActionResult> Active(int? id)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'Context.User'  is null.");
            }
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                _notyf.Warning("User not exist");
                return RedirectToAction(nameof(Index));

            }
            user.Status = EnumValue.AccountStatus.ACTIVE;
            user.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            user.UpdateAt = DateTime.Now;
            try
            {
                _context.Update(user);
                await _context.SaveChangesAsync();
                _notyf.Success("Active account succeed!");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(user.Id))
                {
                    _notyf.Warning("User not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = nameof(UserRole.ADMIN))]
        public async Task<IActionResult> Deactive(int? id)
        {
            if (_context.Users == null)
            {
                return Problem("Entity set 'Context.User'  is null.");
            }
            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                _notyf.Warning("User not exist");
                return RedirectToAction(nameof(Index));
            }
            user.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            user.Status = EnumValue.AccountStatus.DEACTIVE;
            user.UpdateAt = DateTime.Now;
            try
            {
                _context.Update(user);
                await _context.SaveChangesAsync();
                _notyf.Success("Deactive account succeed!");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(user.Id))
                {
                    _notyf.Warning("User not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(int id)
        {
            return (_context.Users?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
