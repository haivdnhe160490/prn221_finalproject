﻿using _05_Trang_10_Hai_FinalProject.Data;
using _05_Trang_10_Hai_FinalProject.EnumValue;
using _05_Trang_10_Hai_FinalProject.Models;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace _05_Trang_10_Hai_FinalProject.Controllers
{
    [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.SALER)}")]
    public class CustomersController : Controller
    {
        private readonly string nameidentifier = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        private readonly Context _context;
        private readonly INotyfService _notyf;

        public CustomersController(Context context, INotyfService notyf)
        {
            _context = context;
            _notyf = notyf;
        }

        // GET: Customers
        public async Task<IActionResult> Index(string? CustomerName, string? CustomerPhone, string? CustomerEmail, string? CustomerContractDate)
        {

            if (_context.Customers == null)
            {
                return Problem("Entity set 'Context.Customer'  is null.");
            }

            var customer = await _context.Customers.Include(c => c.UpdateByNavigation).Include(c => c.CreatedByNavigation).ToListAsync();

            if (CustomerName != null)
            {
                customer = customer.Where(c => c.Name.Contains(CustomerName)).ToList();
            }
            if (CustomerPhone != null)
            {
                customer = customer.Where(c => c.Phone.ToString().Contains(CustomerPhone)).ToList();
            }
            if (CustomerEmail != null)
            {
                customer = customer.Where(c => c.Email.Contains(CustomerEmail)).ToList();
            }

            if (CustomerContractDate != null)
            {
                string createStartDate = CustomerContractDate.Split('-')[0].Trim();
                string createEndDate = CustomerContractDate.Split('-')[1].Trim();
                DateTime StartDate = new DateTime(int.Parse(createStartDate.Split("/")[2]), int.Parse(createStartDate.Split("/")[1]), int.Parse(createStartDate.Split("/")[0]));
                DateTime EndDate = new DateTime(int.Parse(createEndDate.Split("/")[2]), int.Parse(createEndDate.Split("/")[1]), int.Parse(createEndDate.Split("/")[0]));
                customer = customer.Where(c => c.ContractDate >= StartDate && c.ContractDate <= EndDate).ToList();
            }

            if (!User.IsInRole(nameof(UserRole.ADMIN)))
            {
                customer.RemoveAll(c => c.CreatedBy != int.Parse(User.FindFirst(nameidentifier).Value));
            }

            return View(customer);
        }

        // GET: Customers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Customers == null)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            var customer = await _context.Customers.Include(c => c.UpdateByNavigation).Include(c => c.CreatedByNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customer == null)
            {
                _notyf.Warning("User not exist");
                return RedirectToAction(nameof(Index));
            }

            return View(customer);
        }

        // GET: Customers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Address,Phone,Email,Note,Type")] Customer customer)
        {
            customer.CreatedBy = int.Parse(User.FindFirst(nameidentifier).Value);
            customer.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            customer.CreatedAt = DateTime.Now;
            customer.UpdateAt = DateTime.Now;
            customer.ContractDate = DateTime.Now;
            _context.Add(customer);
            _notyf.Success("Customer create succeed");
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Customers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Customers == null)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            var customer = await _context.Customers.Include(c => c.UpdateByNavigation).Include(c => c.CreatedByNavigation).FirstOrDefaultAsync(c=>c.Id == id);
            if (customer == null)
            {
                _notyf.Warning("Customer not exist");
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Address,Phone,Email,Note,Type")] Customer customer)
        {
            if (id != customer.Id)
            {
                _notyf.Error("Somthing when wrong");
                return View(customer);
            }

            var oldCustomer = await _context.Customers.FindAsync(customer.Id);
            if (oldCustomer == null)
            {
                _notyf.Warning("Customer not exist");
                return View(customer);
            }
            try
            {
                oldCustomer.Name = customer.Name;
                oldCustomer.Address = customer.Address;
                oldCustomer.Phone = customer.Phone;
                oldCustomer.Email = customer.Email;
                oldCustomer.Note = customer.Note;
                oldCustomer.Type = customer.Type;
                oldCustomer.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
                oldCustomer.UpdateAt = DateTime.Now;
                _context.Update(oldCustomer);
                await _context.SaveChangesAsync();
                _notyf.Success("Update Success");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustomerExists(customer.Id))
                {
                    _notyf.Warning("Customer not exist");
                    return View(customer);
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Customers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Customers == null)
            {
                return NotFound();
            }

            var customer = await _context.Customers.Include(c => c.UpdateByNavigation).Include(c => c.CreatedByNavigation)  
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Customers == null)
            {
                return Problem("Entity set 'Context.Customer'  is null.");
            }
            var customer = await _context.Customers.FindAsync(id);
            if (customer != null)
            {
                _context.Customers.Remove(customer);
                await _context.SaveChangesAsync();
                _notyf.Success("Remove customer succeed!!");
            }
            else
            {
                _notyf.Warning("Customer not exist");
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Index));
        }

        private bool CustomerExists(int id)
        {
            return (_context.Customers?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
