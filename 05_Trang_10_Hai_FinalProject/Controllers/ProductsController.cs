﻿using _05_Trang_10_Hai_FinalProject.Data;
using _05_Trang_10_Hai_FinalProject.EnumValue;
using _05_Trang_10_Hai_FinalProject.Models;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace _05_Trang_10_Hai_FinalProject.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private readonly Context _context;
        private readonly INotyfService _notyf;

        public ProductsController(Context context, INotyfService notyf)
        {
            _context = context;
            _notyf = notyf;
        }

        public async Task<IActionResult> Index(string? ProductsName, string? CustomerName, int? ProductsCategory)
        {
            var context = _context.Products.Include(p => p.Category).Include(p => p.Customer);

            var products = await context.ToListAsync();
            ViewData["Category"] = new SelectList(_context.Categories, "Id", "Name");
            if (ProductsName != null)
            {
                products = products.Where(c => c.Name.ToLower().Contains(ProductsName.ToLower())).ToList();
            }
            if (CustomerName != null)
            {
                products = products.Where(c => c.Customer.Name.ToLower().Contains(CustomerName.ToLower())).ToList();
            }
            if (ProductsCategory != null && ProductsCategory != -1)
            {
                products = products.Where(c => c.CategoryId == ProductsCategory).ToList();
            }

            return View(products);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Products == null)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            var product = await _context.Products
                .Include(p => p.Category)
                .Include(p => p.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                _notyf.Warning("Product not exist");
                return RedirectToAction(nameof(Index));
            }

            return View(product);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public IActionResult Create()
        {
            ViewData["Category"] = new SelectList(_context.Categories, "Id", "Name");
            ViewData["Customer"] = new SelectList(_context.Customers, "Id", "Name");
            return View();
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,CategoryId,UnitId,CustomerId,Status")] Product product)
        {
            if (ModelState.IsValid)
            {
                _context.Add(product);
                _notyf.Success("Product create succeed");
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Name", product.CategoryId);
            ViewData["CustomerId"] = new SelectList(_context.Customers, "Id", "Name", product.CustomerId);
            return View(product);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Products == null)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                _notyf.Warning("Product not exist");
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Name", product.CategoryId);
            ViewData["CustomerId"] = new SelectList(_context.Customers, "Id", "Name", product.CustomerId);
            return View(product);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,CategoryId,UnitId,CustomerId,Status")] Product product)
        {
            if (id != product.Id)
            {
                _notyf.Error("Somthing when wrong");
                return RedirectToAction(nameof(Index));
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(product);
                    _notyf.Success("Update succeed");
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.Id))
                    {
                        _notyf.Error("Somthing when wrong");
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Id", product.CategoryId);
            ViewData["CustomerId"] = new SelectList(_context.Customers, "Id", "Id", product.CustomerId);
            return View(product);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Products == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .Include(p => p.Category)
                .Include(p => p.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Products == null)
            {
                return Problem("Entity set 'Context.Product'  is null.");
            }
            var product = await _context.Products.FindAsync(id);
            if (product != null)
            {
                _notyf.Success("Remove succeed");
                _context.Products.Remove(product);
            }
            else
            {
                _notyf.Warning("Product not exist");
                return RedirectToAction(nameof(Index));
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        private bool ProductExists(int id)
        {
            return (_context.Products?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
