﻿using _05_Trang_10_Hai_FinalProject.Data;
using _05_Trang_10_Hai_FinalProject.EnumValue;
using _05_Trang_10_Hai_FinalProject.Models;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace _05_Trang_10_Hai_FinalProject.Controllers
{
    [Authorize]
    public class ImportsController : Controller
    {
        private readonly string nameidentifier = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        private readonly Context _context;
        private readonly INotyfService _notyf;

        public ImportsController(Context context, INotyfService notyf)
        {
            _context = context;
            _notyf = notyf;
        }

        public async Task<IActionResult> Index(DateTime? startCreatedAt, DateTime? endCreatedAt,
            DateTime? startUpdatedAt, DateTime? endUpdatedAt)
        {
            if (_context.Imports == null)
            {
                return Problem("Entity set 'Context.Import'  is null.");
            }

            var context = _context.Imports.Include(i => i.Customer).Include(i => i.UpdateByNavigation).Include(i => i.CreatedByNavigation);
            var import = await context.ToListAsync();

            if (startCreatedAt != null)
            {
                import = import.Where(i => i.CreatedAt >= startCreatedAt).ToList();
            }

            if (endCreatedAt != null)
            {
                import = import.Where(i => i.CreatedAt <= endCreatedAt).ToList();
            }

            if (startUpdatedAt != null)
            {
                import = import.Where(i => i.UpdateAt >= startUpdatedAt).ToList();
            }

            if (endUpdatedAt != null)
            {
                import = import.Where(i => i.UpdateAt <= endUpdatedAt).ToList();
            }

            return View(import);

        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Imports == null)
            {
                return NotFound();
            }

            var import = await _context.Imports.Include(i => i.ImportInfos).ThenInclude(i => i.Product).Include(i => i.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (import == null)
            {
                return NotFound();
            }

            ViewData["Customers"] = new SelectList(_context.Customers.Where(c => c.Products.Count() > 0), "Id", "Name");
            ViewData["Products"] = new SelectList(_context.Products, "Id", "Name");
            return View(import);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public IActionResult Create()
        {
            Import import = new Import();
            import.ImportInfos = new List<ImportInfo>();
            import.ImportInfos.Add(new ImportInfo());
            ViewData["Customers"] = new SelectList(_context.Customers.Where(c => c.Products.Count() > 0), "Id", "Name");
            return View("Create", import);
        }

        [HttpPost]
        public IActionResult GetCusProduct(int cusId)
        {
            var res = _context.Products.Where(p => p.CustomerId == cusId);
            return Ok(res);
        }

        [HttpPost]
        public IActionResult GetProductType(int poId)
        {
            var res = _context.Products.Find(poId);
            return Ok(res);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Import import)
        {
            import.CreatedBy = int.Parse(User.FindFirst(nameidentifier).Value);
            import.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            import.UpdateAt = DateTime.Now;
            import.CreatedAt = DateTime.Now;

            _context.Add(import);
            await _context.SaveChangesAsync();
            _notyf.Success("Create import succeed");
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Imports == null)
            {
                _notyf.Error("Something went wrong");
                return RedirectToAction(nameof(Index));
            }

            var import = await _context.Imports.Include(i => i.ImportInfos).ThenInclude(i => i.Product).Include(i => i.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);
            ViewData["Customers"] = new SelectList(_context.Customers.Where(c => c.Products.Count() > 0), "Id", "Name");
            ViewData["Products"] = new SelectList(_context.Products.Where(p => p.CustomerId == import.CustomerId), "Id", "Name");
            if (import == null)
            {
                _notyf.Warning("Import not exist");
                return RedirectToAction(nameof(Index));
            }
            return View(import);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Import import)
        {
            if (id != import.Id)
            {
                _notyf.Warning("Import not exist");
                return RedirectToAction(nameof(Index));
            }

            var oldImport = await _context.Imports
                            .FirstOrDefaultAsync(m => m.Id == id);
            if (oldImport == null)
            {
                _notyf.Warning("Import not exist");
                return RedirectToAction(nameof(Index));
            }
            oldImport.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            oldImport.UpdateAt = DateTime.Now;
            oldImport.Status = import.Status;
            oldImport.CustomerId = import.CustomerId;
            oldImport.ImportInfos = import.ImportInfos;
            try
            {
                _context.Update(oldImport);
                _notyf.Success("Update succeed");
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImportExists(import.Id))
                {
                    _notyf.Warning("Import not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));

        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Imports == null)
            {
                return NotFound();
            }

            var import = await _context.Imports
                .FirstOrDefaultAsync(m => m.Id == id);
            if (import == null)
            {
                return NotFound();
            }

            return View(import);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Imports == null)
            {
                _notyf.Error("Something went wrong");
                return RedirectToAction(nameof(Index));
            }
            var import = await _context.Imports.FindAsync(id);
            if (import != null)
            {
                _context.Imports.Remove(import);
            }
            else
            {
                _notyf.Warning("Import not exist");
                return RedirectToAction(nameof(Index));
            }
            _notyf.Success("Delete succeed");
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Active(int? id)
        {
            if (_context.Imports == null)
            {
                _notyf.Error("Something went wrong");
                return RedirectToAction(nameof(Index));
            }
            var import = await _context.Imports
                .FirstOrDefaultAsync(i => i.Id == id);
            if (import == null)
            {
                _notyf.Warning("Import not exist");
                return RedirectToAction(nameof(Index));

            }
            import.Status = EnumValue.ProductStatus.ACTIVE;
            import.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            import.UpdateAt = DateTime.Now;
            try
            {
                _context.Update(import);
                await _context.SaveChangesAsync();
                _notyf.Success("Active Import succeed!");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImportExists(import.Id))
                {
                    _notyf.Warning("Import not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Deactive(int? id)
        {
            if (_context.Imports == null)
            {
                _notyf.Warning("Something went wrong");
                return RedirectToAction(nameof(Index));
            }
            var import = await _context.Imports
                .FirstOrDefaultAsync(i => i.Id == id);
            if (import == null)
            {
                _notyf.Warning("Import not exist");
                return RedirectToAction(nameof(Index));
            }
            import.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            import.Status = EnumValue.ProductStatus.DEACTIVE;
            import.UpdateAt = DateTime.Now;
            try
            {
                _context.Update(import);
                await _context.SaveChangesAsync();
                _notyf.Success("Deactive import succeed!");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImportExists(import.Id))
                {
                    _notyf.Warning("Import not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }

        private bool ImportExists(int id)
        {
            return (_context.Imports?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
