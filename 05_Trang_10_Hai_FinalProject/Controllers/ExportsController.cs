﻿using _05_Trang_10_Hai_FinalProject.Data;
using _05_Trang_10_Hai_FinalProject.EnumValue;
using _05_Trang_10_Hai_FinalProject.Models;
using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace _05_Trang_10_Hai_FinalProject.Controllers
{
    [Authorize]
    public class ExportsController : Controller
    {
        private readonly string nameidentifier = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        private readonly INotyfService _notyf;
        private readonly Context _context;

        public ExportsController(Context context, INotyfService notyf)
        {
            _context = context;
            _notyf = notyf;
        }

        public async Task<IActionResult> Index(DateTime? startCreatedAt, DateTime? endCreatedAt,
            DateTime? startUpdatedAt, DateTime? endUpdatedAt)
        {
            if (_context.Exports == null)
            {
                return Problem("Entity set 'Context.Export'  is null.");
            }

            var context = _context.Exports.Include(e => e.Customer).Include(e => e.CreatedByNavigation).Include(e => e.UpdateByNavigation);
            var export = await context.ToListAsync();

            if (startCreatedAt != null)
            {
                export = export.Where(c => c.CreatedAt >= startCreatedAt).ToList();
            }

            if (endCreatedAt != null)
            {
                export = export.Where(c => c.CreatedAt <= endCreatedAt).ToList();
            }

            if (startUpdatedAt != null)
            {
                export = export.Where(c => c.UpdateAt >= startUpdatedAt).ToList();
            }

            if (endUpdatedAt != null)
            {
                export = export.Where(c => c.UpdateAt <= endUpdatedAt).ToList();
            }

            return View(export);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Exports == null)
            {
                return NotFound();
            }

            var export = await _context.Exports.Include(e => e.ExportInfos).ThenInclude(e => e.Product).Include(e => e.Customer)
                .FirstOrDefaultAsync(m => m.Id == id);
            ViewData["Customers"] = new SelectList(_context.Customers, "Id", "Name");
            ViewData["Products"] = new SelectList(_context.Products, "Id", "Name");
            if (export == null)
            {
                return NotFound();
            }
            return View(export);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public IActionResult Create()
        {
            Export export = new Export();
            export.ExportInfos = new List<ExportInfo>();
            export.ExportInfos.Add(new ExportInfo());
            ViewData["Customers"] = new SelectList(_context.Customers, "Id", "Name");
            ViewData["Products"] = new SelectList(_context.Products, "Id", "Name");
            return View(export);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Export export)
        {
            export.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            export.UpdateAt = DateTime.Now;
            export.CreatedBy = int.Parse(User.FindFirst(nameidentifier).Value);
            export.CreatedAt = DateTime.Now;

            _context.Add(export);
            await _context.SaveChangesAsync();
            _notyf.Warning("Create export succeed");
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Exports == null)
            {
                _notyf.Error("Something went wrong");
                return RedirectToAction(nameof(Index));
            }

            var export = await _context.Exports.Include(e => e.ExportInfos).ThenInclude(e => e.Product).Include(e => e.Customer)
            .FirstOrDefaultAsync(m => m.Id == id);
            ViewData["Customers"] = new SelectList(_context.Customers, "Id", "Name");
            ViewData["Products"] = new SelectList(_context.Products, "Id", "Name");
            if (export == null)
            {
                _notyf.Warning("Export not exist");
                return RedirectToAction(nameof(Index));
            }
            return View(export);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Export export)
        {
            if (id != export.Id)
            {
                _notyf.Warning("Export not exist");
                return RedirectToAction(nameof(Index));
            }
            var oldExport = await _context.Exports
            .FirstOrDefaultAsync(m => m.Id == id);
            if (oldExport == null)
            {
                _notyf.Warning("Export not exist");
                return RedirectToAction(nameof(Index));
            }
            oldExport.Status = export.Status;
            oldExport.CustomerId = export.CustomerId;
            oldExport.ExportInfos = export.ExportInfos;
            oldExport.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            oldExport.UpdateAt = DateTime.Now;
            try
            {
                _context.Update(oldExport);
                _notyf.Success("Update succeed");
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExportExists(export.Id))
                {
                    _notyf.Warning("Export not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Exports == null)
            {
                _notyf.Warning("Somethings went wrong");
                return RedirectToAction(nameof(Index));
            }

            var export = await _context.Exports
                .FirstOrDefaultAsync(m => m.Id == id);
            if (export == null)
            {
                _notyf.Warning("Export not exist");
                return RedirectToAction(nameof(Index));
            }

            return View(export);
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Exports == null)
            {
                _notyf.Error("Something went wrong");
                return RedirectToAction(nameof(Index));
            }
            var export = await _context.Exports.FindAsync(id);
            if (export != null)
            {
                _context.Exports.Remove(export);
                _notyf.Success("Delete succeed");
            }
            else
            {
                _notyf.Warning("Export not exist");
                return RedirectToAction(nameof(Index));
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Active(int? id)
        {
            if (_context.Exports == null)
            {
                _notyf.Error("Something went wrong");
                return RedirectToAction(nameof(Index));
            }
            var export = await _context.Exports
                .FirstOrDefaultAsync(i => i.Id == id);
            if (export == null)
            {
                _notyf.Warning("Export not exist");
                return RedirectToAction(nameof(Index));

            }
            export.Status = EnumValue.ProductStatus.ACTIVE;
            export.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            export.UpdateAt = DateTime.Now;
            try
            {
                _context.Update(export);
                await _context.SaveChangesAsync();
                _notyf.Success("Active export succeed!");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExportExists(export.Id))
                {
                    _notyf.Warning("Export not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = $"{nameof(UserRole.ADMIN)},{nameof(UserRole.WAREHOUSE_MANGEMENT)}")]
        public async Task<IActionResult> Deactive(int? id)
        {
            if (_context.Exports == null)
            {
                _notyf.Warning("Something went wrong");
                return RedirectToAction(nameof(Index));
            }
            var export = await _context.Exports
                .FirstOrDefaultAsync(i => i.Id == id);
            if (export == null)
            {
                _notyf.Warning("Export not exist");
                return RedirectToAction(nameof(Index));
            }
            export.UpdateBy = int.Parse(User.FindFirst(nameidentifier).Value);
            export.Status = EnumValue.ProductStatus.DEACTIVE;
            export.UpdateAt = DateTime.Now;
            try
            {
                _context.Update(export);
                await _context.SaveChangesAsync();
                _notyf.Success("Deactive export succeed!");
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExportExists(export.Id))
                {
                    _notyf.Warning("Export not exist");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }

        private bool ExportExists(int id)
        {
            return (_context.Exports?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
