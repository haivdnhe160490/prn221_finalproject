﻿using System.ComponentModel.DataAnnotations;

namespace _05_Trang_10_Hai_FinalProject.EnumValue
{
    public enum CustomerType
    {
        [Display(Name = "VIP")] VIP,
        [Display(Name = "NORMAL")] NORMAL
    }
}
