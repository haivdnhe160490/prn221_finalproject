﻿using System.ComponentModel.DataAnnotations;

namespace _05_Trang_10_Hai_FinalProject.EnumValue
{
    public enum ProductUnit
    {
        [Display(Name = "PIECES")] PIECES,
        [Display(Name = "KILOGRAM")] KILOGRAM,
        [Display(Name = "METRE")] METRE
    }
}
