﻿using System.ComponentModel.DataAnnotations;

namespace _05_Trang_10_Hai_FinalProject.EnumValue
{
    public enum UserRole
    {
        [Display(Name = "ADMIN")] ADMIN,
        [Display(Name = "WAREHOUSE MANGEMENT")] WAREHOUSE_MANGEMENT,
        [Display(Name = "SALER")] SALER
    }
}
