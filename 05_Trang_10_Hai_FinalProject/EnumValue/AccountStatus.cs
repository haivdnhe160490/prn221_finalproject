﻿using System.ComponentModel.DataAnnotations;

namespace _05_Trang_10_Hai_FinalProject.EnumValue
{
    public enum AccountStatus
    {
        [Display(Name = "ACTIVE")] ACTIVE,
        [Display(Name = "DEACTIVE")] DEACTIVE
    }
}
