﻿var toastbtnDeletes = document.querySelectorAll('#toastbtnDelete');
toastbtnDeletes.forEach(function (button, index) {
    button.addEventListener('click', function (event) {
        var id = button.getAttribute("data-item-id");
        var name = button.getAttribute("data-item-name");

        var toastElList = [].slice.call(document.querySelectorAll('.toast'))
        var toastList = toastElList.map(function (toastEl) {
            return new bootstrap.Toast(toastEl)
        })

        toastList.filter(function (toast) {
            return toastElList[toastList.indexOf(toast)].classList.contains('div_delete');
        }).forEach(function (toast) {
            toast._element.querySelector('.toast-body #Id').value = id
            toast._element.querySelector('.toast-body .item_name_delete').innerHTML = name
            toast.show();
        });
    });
});

$("#dropdownCustomer").change(function () {
    var selectedValue = $(this).val();
    $(this)
    $("#CodesTable").show();
    $("[id=item_Product_UnitId], [id=item_InPrice],[id=item_OutPrice],[id=item_Count]").val("");
    $.ajax({
        type: "POST",
        url: `/Imports/GetCusProduct?cusId=${selectedValue}`,
        success: function (results) {
            $("[id=productCusList]").each(function () {
                var secondDropdown = $(this);
                secondDropdown.empty();
                secondDropdown.append($('<option>'));
                $.each(results, function (index, result) {
                    secondDropdown.append($('<option>', {
                        value: result.Id,
                        text: result.Name
                    }));
                });
            });

        }
    });
});

$("#importInfoList").on('change', '#productCusList', function () {
    var selectedProduct = $(this).val();
    var parentRow = $(this).closest('tr');
    var itemProductUnitIdInput = parentRow.find('#item_Product_UnitId');
    $.ajax({
        type: "POST",
        url: `/Imports/GetProductType?poId=${$(this).val()}`,
        success: function (results) {
            itemProductUnitIdInput.val(results.UnitId)
            console.log(results);
        }
    });
});

$("#importInfoList").on('input', '#ImportInfos__InPrice', function () {
    var inputPrice = $(this).val();
    var parentRow = $(this).closest('tr');
    var outprice = parentRow.find('#ImportInfos__OutPrice');
    var price = parseInt(inputPrice, 10)
    if (!isNaN(price)) {
        outprice.val(price + (($(this).val() * (10 / 100))));
    } else {
        $(this).val(null);
    }
});

$(function () {
    ScirptUser();
    ScirptCustomer();
    ScirptProduct();
});

function ScirptUser() {
    const url = new URL(window.location.href);
    const searchParams = new URLSearchParams(url.search);
    const CreatedUserAt = searchParams.get("CreatedUserAt");
    const UpdatedUserAt = searchParams.get("UpdatedUserAt");
    const roleUser = searchParams.get("roleUser");
    const emailUser = searchParams.get("emailUser");

    $('input[name="CreatedUserAt"]').val(CreatedUserAt ? decodeURIComponent(CreatedUserAt) : "");
    $('input[name="UpdatedUserAt"]').val(UpdatedUserAt ? decodeURIComponent(UpdatedUserAt) : "");
    $('input[name="emailUser"]').val(emailUser ? decodeURIComponent(emailUser) : "");
    $('select[name="roleUser"]').val(roleUser ? decodeURIComponent(roleUser) : -1);

    $('input[name="CreatedUserAt"]').daterangepicker({
        opens: 'center',
        "autoApply": true,
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });
    $('input[name="CreatedUserAt"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('input[name="UpdatedUserAt"]').daterangepicker({
        opens: 'center',
        "autoApply": true,
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });
    $('input[name="UpdatedUserAt"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });
}

function ScirptCustomer() {
    const url = new URL(window.location.href);
    const searchParams = new URLSearchParams(url.search);
    const CustomerName = searchParams.get("CustomerName");
    const CustomerPhone = searchParams.get("CustomerPhone");
    const CustomerEmail = searchParams.get("CustomerEmail");
    const CustomerContractDate = searchParams.get("CustomerContractDate");
    console.log(decodeURIComponent(searchParams));

    $('input[name="CustomerName"]').val(CustomerName ? decodeURIComponent(CustomerName) : "");
    $('input[name="CustomerPhone"]').val(CustomerPhone ? decodeURIComponent(CustomerPhone) : "");
    $('input[name="CustomerEmail"]').val(CustomerEmail ? decodeURIComponent(CustomerEmail) : "");
    $('input[name="CustomerContractDate"]').val(CustomerContractDate ? decodeURIComponent(CustomerContractDate) : "");

    $('input[name="CustomerContractDate"]').daterangepicker({
        opens: 'center',
        "autoApply": true,
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });
    $('input[name="CustomerContractDate"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });
}

function ScirptProduct() {
    const url = new URL(window.location.href);
    const searchParams = new URLSearchParams(url.search);
    const CustomerName = searchParams.get("CustomerName");
    const ProductsName = searchParams.get("ProductsName");
    const ProductsCategory = searchParams.get("ProductsCategory");

    $('input[name="CustomerName"]').val(CustomerName ? decodeURIComponent(CustomerName) : "");
    $('input[name="ProductsName"]').val(ProductsName ? decodeURIComponent(ProductsName) : "");
    $('select[name="ProductsCategory"]').val(ProductsCategory ? decodeURIComponent(ProductsCategory) : -1);
}

function AddItem(btn) {
    var table;
    table = document.getElementById('CodesTable');
    var rows = table.getElementsByTagName('tr');
    var rowOuterHtml = rows[rows.length - 1].outerHTML;
    var lastrowIdx = rows.length - 2;
    var nextrowIdx = eval(lastrowIdx) + 1;

    rowOuterHtml = rowOuterHtml.replaceAll('_' + lastrowIdx + '_', '_' + nextrowIdx + '_');
    rowOuterHtml = rowOuterHtml.replaceAll('[' + lastrowIdx + ']', '[' + nextrowIdx + ']');
    rowouterHtml = rowOuterHtml.replaceAll('-' + lastrowIdx, '-' + nextrowIdx);

    var newRow = table.insertRow();

    newRow.innerHTML = rowOuterHtml;

    var x = document.getElementsByTagName("INPUT");

    for (var cnt = 0; cnt < x.length; cnt++) {
        if (x[cnt].type == "text" && x[cnt].id.indexOf('_' + nextrowIdx + '_') > 0) {
            x[cnt].value = '';
        }
        else if (x[cnt].type == "number" && x[cnt].id.indexOf('_' + nextrowIdx + '_') > 0) {
            x[cnt].value = 0;
        }
    }

}

function DeleteItem(btn) {
    var table = document.getElementById('CodesTable');
    var rows = table.getElementsByTagName('tr');
    if (rows.length == 2) {
        alert("This Row Cannot Be Deleted");
        return;
    }
    $(btn).closest('tr').remove()
};

function rebindvalidators() {
    var $form = $("#import_form_infor");
    $form.unbind();
    $form.data("validator", null);
    $.validator.unobtrusive.parse($form);
    $form.validate($form.data("unobtrusiveValidation").options);
}